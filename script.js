//create a window when click on the button id=addFavorite
function displayWindowAdd() {
    let div = document.body.querySelector("#main")
    let window = document.createElement("div");
    window.id = "windowAdd";
    div.appendChild(window)

    //add a h2 to the window
    let h2 = document.createElement("h2");
    h2.innerHTML = "Ajouter un favori";
    window.appendChild(h2);

    //add a img to the window
    let img = new Image();
    img.src = "img/Croix.png";
    img.id = "close";
    img.onclick = function () {
        window.remove();
    }
    window.appendChild(img);


    //create a label
    let labelTitle = document.createElement("label");
    labelTitle.htmlFor = "name";
    labelTitle.innerHTML = "Nom du site";
    window.appendChild(labelTitle);

    //create a input for title
    let inputTitle = document.createElement("input");
    inputTitle.type = "text";
    inputTitle.id = "title";
    window.appendChild(inputTitle);

    //create a label for url
    let labelUrl = document.createElement("label");
    labelUrl.htmlFor = "url";
    labelUrl.innerHTML = "Url du site";
    window.appendChild(labelUrl);

    //create a input for url
    let inputUrl = document.createElement("input");
    inputUrl.type = "text";
    inputUrl.id = "url";
    window.appendChild(inputUrl);

    //create a button for add
    let buttonAdd = document.createElement("button");
    buttonAdd.id = "createFavorite";
    buttonAdd.setAttribute("onclick", "addFavorite()");
    buttonAdd.innerHTML = "Ajouter";
    window.appendChild(buttonAdd);
}

function displaySettingWindow() {
    let div = document.body.querySelector("#main")
    let window = document.createElement("div");
    window.id = "windowSetting";
    div.appendChild(window)

    //add a h2 to the window
    let h2 = document.createElement("h2");
    h2.innerHTML = "Retirer ou modifier un favori";
    window.appendChild(h2);

    //add a img to the window
    let img = new Image();
    img.src = "img/Croix.png";
    img.id = "close";
    img.onclick = function () {
        displayList();
        window.remove();
    }
    window.appendChild(img);

    for (const aFavorite in localStorage) {
        let favorite = JSON.parse(localStorage.getItem(aFavorite));
        if (favorite != null) {
            let favoriteDiv = document.createElement("div");
            //Input for a title
            let inputTitle = document.createElement("input");
            inputTitle.type = "text";
            inputTitle.className = "title";
            inputTitle.value = favorite.title;
            favoriteDiv.appendChild(inputTitle);
            
            //Input for a url
            let inputUrl = document.createElement("input");
            inputUrl.type = "text";
            inputUrl.className = "url";
            inputUrl.value = favorite.url;
            favoriteDiv.appendChild(inputUrl);
            
            inputUrl.onchange = function () {
                localStorage.setItem(aFavorite, JSON.stringify({
                    title: inputTitle.value,
                    url: inputUrl.value,
                    image : "https://s2.googleusercontent.com/s2/favicons?sz=64&domain_url=" + inputUrl.value
                }));
                displayList();
            }
            inputTitle.onchange = function () {
                localStorage.setItem(aFavorite, JSON.stringify({
                    title: inputTitle.value,
                    url: inputUrl.value,
                    image : "https://s2.googleusercontent.com/s2/favicons?sz=64&domain_url=" + inputUrl.value
                }));
                displayList();
            }
            let img = new Image();
            img.src = "img/Croix.png";
            img.className = "removeFavorite";
            img.id=aFavorite;
            img.onclick = function () {
                removeFavorite(img.id);
                displayList();
                window.remove();
            }
            favoriteDiv.appendChild(img);

            window.appendChild(favoriteDiv);
        }
    }
    //create a button for add
    let buttonAdd = document.createElement("button");
    buttonAdd.id = "createFavorite";
    buttonAdd.onclick = function () {
        displayList();
        window.remove();
    }
    buttonAdd.innerHTML = "Terminer";
    window.appendChild(buttonAdd);
}

function addFavorite() {
    var title = document.getElementById("title").value;
    var url = document.getElementById("url").value;
    if (title != "" && url != "") {
        var favorite = {
            title: title,
            url: url,
            image: "https://s2.googleusercontent.com/s2/favicons?sz=64&domain_url=" + url
        };
        var json = JSON.stringify(favorite);
        localStorage.setItem(title, json);
        alert("Favorite added");
        let window = document.body.querySelector("#windowAdd");
        window.remove();
    } else {
        alert("Rien dedans");
    }
    displayList()
}

//print the list of favorite
function displayList() {
    let div = document.body.querySelector("#favorites")
    while (div.firstChild) {
        div.removeChild(div.lastChild);
    }
    for (const aFavorite in localStorage) {
        let favorite = JSON.parse(localStorage.getItem(aFavorite));
        if (favorite) {

            let divFavorite = document.createElement("div");
            divFavorite.className = "favorite";
            divFavorite.setAttribute("onclick", "window.open('" + favorite.url + "', '_blank')");
            div.appendChild(divFavorite);

            let img = document.createElement("img");
            img.src = favorite.image;
            divFavorite.appendChild(img);

            let h2 = document.createElement("h2");
            h2.innerHTML = favorite.title;
            divFavorite.appendChild(h2);
        }
    }

}


//create a window when click on the button id=removeFavorite

function removeFavorite(title) {
    localStorage.removeItem(title);
    alert("Favorite removed");
}
/*
<div className="connection-form">
            <form action="" onSubmit={handleSubmit} id="login-form">
                <h2 className="message-error"> </h2>
                <label htmlFor="username">Identifiant</label>
                <input type="text" autoComplete='username' name='username' id="username" value={username} onChange={
                    (e) => { setUsername(e.target.value) }
                } />
                <label htmlFor="password">Mot de passe</label>
                <input type="password" autoComplete='current-password' name='password' id="password" value={password} onChange={
                    (e) => setPassword(e.target.value)
                } />
                <input type="submit" value="Se connecter" id="submit" />
            </form>
        </div>





*/



//
